FROM nginx:alpine

RUN chmod 777 /var/log/nginx && \
    chmod 777 /var/cache/nginx && \
    chmod 777 /var/run && \
    rm -rf /var/log/nginx/* && \
    rm -rf /var/cache/nginx/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    apk add --no-cache dnsmasq python2 py-setuptools py-pip && \
    pip install supervisor && \
    apk del --no-cache py-pip

ADD supervisord.conf /etc/supervisord.conf
ADD nginx.conf /etc/nginx/nginx.conf

EXPOSE 8080
CMD ["/usr/bin/supervisord", "-n"]
