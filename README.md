# Scaligrade

Codename: 4Scale. A simple proxy to sit between 3scale and our review deployments.

## Usage

Scaligrade is powered primarily by two headers. First is `X-4Scale-Branch` to specify the branch name in our review environment, so the proxy knows what api service to forward you to. Optionally you can also add the `X-4Scale-Debug` header with any value to output debug headers about the handled request.

Simply include one, or more headers (as needed), and Scaligrade will get you where you need to go.
